import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

_url = 'https://swapi.dev/api'

  constructor(
    private http: HttpClient
  ) {
   }

   getVehicles(){
    return this.http.get(`${this._url}/vehicles/`); 
   }

   getPeople(){
    return this.http.get(`${this._url}/people/`); 
   }
}

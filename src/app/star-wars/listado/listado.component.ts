import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { People } from 'src/app/interfaces/People.interface';
import { InfoService } from 'src/app/services/info.service';
//Data table with sorting, pagination, and filtering.
@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements AfterViewInit {
  vehicles:any;
  displayedColumns: string[] = ['name', 'birth_year', 'eye_color', 'gender','actions'];
  //dataSource!:MatTableDataSource<People>;
  dataSource:any;
  people:People[] =[];
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private infoStarWars: InfoService) {  
    this.dataSource = new MatTableDataSource(this.people);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  get_People(){
    this.infoStarWars.getPeople().subscribe( (data: any) =>{
      this.people = data.results;
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit(): void {
    this.get_People();
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ListadoComponent } from './listado/listado.component';
import { InfoService } from '../services/info.service';

import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { ListComponent } from './people/list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

@NgModule({
    
    declarations:[
        ListadoComponent,
        ListComponent,
    ],
    exports:[
        ListadoComponent,
        ListComponent
    ],

    imports:[
        CommonModule,
        MatTableModule,
        HttpClientModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule
    ],
    providers:[
        InfoService
    ]

})

export class StarWarsModule{

}
interface Vehicle {
    name: string,
    model: string,
    cargo_capacity: number,
    consumables: string,
    cost_in_credits: number,
    passengers: number 
  }
  
import { Component } from  '@angular/core';


@Component({
    selector: 'app-index',
    templateUrl: 'index.component.html'
})


export class IndexComponent{
    nombre: string = 'Ironman';
    edad: number = 45;
    
    obtenerNombre(): string{
    return `${ this.nombre} - ${this.edad }`;
    }

    borrarHeroe(indice: number){
        var arr = ["orange", "mango", "banana", "sugar", "tea"];  
        var removed = arr.splice(2, 0, "water");  
        // console.log("After adding 1: " + arr );  
        // console.log("removed is: " + removed); 
                
        removed = arr.splice(indice, 1);  
        console.log("After removing 1: " + arr );  
        console.log("removed is: " + removed);
    }
}